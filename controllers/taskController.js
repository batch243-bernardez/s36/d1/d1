// Controllers contain the functions and business logic of our Express JS Application
// All the operations will be placed in this file
	const Task = require("../models/task");


// Controller function for GETTING all the tasks
	module.exports.getAllTasks=()=>{
 			return Task.find({}).then(result =>{
 				return result
 			})
 		}



// Controller Function for CREATING a task
	module.exports.createTask = (requestBody) =>{

			//Creates a task object based on the Mongoose model "Task"
			let newTask = new Task({

				//Sets the "name" property with the value receive from the client/Postman
				name: requestBody.name
			})

		
		 return newTask.save().then((task,error) =>{

				 	if(error){
				 		console.log(error);

				 		return false;
				 	} else {

				 		return task;
				 	}
				 })
			}



// Cotroller function for DELETING a task

/*
	Business Logic
	1. Look for the task with the corresponding id provided in the URL/route
	2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

	module.exports.deleteTask =(taskId) => {

				return Task.findByIdAndRemove(taskId).then((removedTask, err)=>{

					if(err){
						console.log(err)
						return false;
					}else{
						return removedTask;
					}
				})
			}



// Controller function for UPDATING a task
/*
	Business Logic
	1. Get the task with the id using the Mongoose method "findById"
	2. Replace the task's name returned from the database with the "name" property from the request body
	3. Save the task
*/
	module.exports.updateTask =(taskId, newContent) =>{

				return Task.findById(taskId).then((result, error) =>{
					if(error){
						console.log(error)
						return false
					}
						result.name = newContent.name;

						return result.save().then((updatedTask, saveErr) =>{

							if(saveErr){
								console.log(saveErr)
								return false
							}else{

								return updatedTask
							}
						})		
			})
		}





