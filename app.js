// Setup Dependencies
const express = require('express');
const mongoose = require('mongoose');


// Create an application using express function
const app = express();
const port = 3001;


// This allows us to use all the routes defined in taskRoute.js
const taskRoute = require("./routes/taskRoute");

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Allows all the task routes created in "taskRoute.js" file to use "/tasks" route
app.use("/tasks", taskRoute)

// Database connection

// Connection to MONGODB
mongoose.connect("mongodb+srv://mpyb:mpyb@zuittbatch243.kotcmj5.mongodb.net/B243-to-do?retryWrites=true&w=majority", 
	{ 
				  useNewUrlParser:true,
				  useUnifiedTopology: true
				}
			);




app.listen(port,() =>console.log(`Server running at port ${port}`));

