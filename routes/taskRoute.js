const express = require('express')

// Create a Router Instance that functions as a middleware and routing system
	const router = express.Router();

// Connecting to controller
	const taskController = require("../controllers/taskController");

// ROUTES

// Route to get all the tasks
/*
	- This route expects to receive a GET request at the URL "/tasks"
	- "taskController.getAllTasks()" - it invokes the getAllTasks function from the taskController.js file and send the reult back to tech client or portman
	- "module.exports" - use to export the router object to use in the app.js
*/
	router.get("/",(req,res)=>{
			//Invokes the 'getAllTasks' function from the "taksController.js"file and send the result back to teh client/Postman
			taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
		})



// Route to create a new task
// This route will receive a POST request at the UTL "/tasks"

	router.post("/",(req,res)=>{
			//if information will be coming from the client side the data can be accessed from the request body
			taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
		})


// Route to delete a task - DELETE request

	router.delete("/:id", (req, res) =>{

			taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
		});



// Route to update task - PUT request

	router.put("/:id",(req, res)=>{

			taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
		});


// Use "module.exports" to export the router object to use in the app.js
	module.exports = router;